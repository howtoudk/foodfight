/*	Corey Dixon
 *	3/25/14
 *	FoodPawn Class
 * 	Description: Pawn Class for the FoodGame
 */
 
class FoodPawn extends UTPawn
	placeable
	ClassGroup(Food);

	
var PointLightComponent BeamLight;

simulated event Touch( Actor Other, PrimitiveComponent OtherComp, Vector HitLocation, Vector HitNormal )
{	
	super.Touch( Other, OtherComp, HitLocation, HitNormal );
	
	if(Other.isA('Sammich'))
	{
		Health += 10;
		GroundSpeed += 100;
		`log(self @ "Ate" @ Other @ "Health =" @ Health);
	}
}

function AddDefaultInventory()
{
    //InvManager.CreateInventory(class'Plague.PlagueWeapon'); //InvManager is the pawn's InventoryManager
	InvManager.CreateInventory(class'Food.ShockRifle_Fast');
}

function DeactivateSpawnProtection()
{
	if ( Role < ROLE_Authority )
		return;
	if ( !bSpawnDone )
	{
		bSpawnDone = true;
		bSpawnIn = true;
		if (BodyMatColor == SpawnProtectionColor)
		{
			ClearBodyMatColor();
		}
		SpawnTime = -100000;
	}
}

defaultproperties
{
	InventoryManagerClass=class'FoodInventoryManager'
	
	Begin Object Class=PointLightComponent Name=LightComponentB
		bEnabled=true
		Brightness=0.25
		CastShadows=false
        LightColor=(R=0,G=0,B=255,A=255)
        Radius=2048
    End Object
    BeamLight=LightComponentB
	Components.Add(LightComponentB)
}