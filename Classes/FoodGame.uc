/*	Corey Dixon
 *	3/25/14
 *	FoodFight Game Class
 *	Description: GameClass for FoodFight Game, Game Logic belongs here.
 */
 
class FoodGame extends UTGame
	ClassGroup(Food);

	
function AddInitialBots()
{
	`log("Adding Initial Bots!!");
}


auto state Begin
{

}

event AddDefaultInventory(Pawn P)
{
    // Allow the pawn itself to modify its inventory
    P.AddDefaultInventory();

	if ( P.InvManager == None )
	{
		`warn("GameInfo::AddDefaultInventory - P.InvManager == None");
	}
}

/* StartMatch()
Start the game - inform all actors that the match is starting, and spawn player pawns
*/
function StartMatch()
{
	local Actor A;

	if ( MyAutoTestManager != None )
	{
		MyAutoTestManager.StartMatch();
	}

	// tell all actors the game is starting
	ForEach AllActors(class'Actor', A)
	{
		A.MatchStarting();
	}

	// start human players first
	StartHumans();

	// start AI players
	StartBots();

	bWaitingToStartMatch = false;

	//StartOnlineGame();

	// fire off any level startup events
	WorldInfo.NotifyMatchStarted();
}

defaultproperties
{
	bUseClassicHUD=true
	DefaultPawnClass=class'FoodPawn'
	PlayerControllerClass=class'FoodPlayerController'
	HUDType=class'FoodHUD'
	bRestartLevel=false
	bWaitingToStartMatch=true
	bDelayedStart=false
}