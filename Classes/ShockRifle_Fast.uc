/**
 *	ShockRifle_Fast
 *
 *	Creation date: 16/05/2012 13:32
 *	Copyright 2012, Zimhey
 */
class ShockRifle_Fast extends UTWeap_ShockRifle;



defaultproperties
{
	WeaponProjectiles(1)=class'NoDmgShockBall'
	InstantHitDamage(0)=0
	FireInterval(0)=+0.05
	FireInterval(1)=+0.05
	ShotCost(0)=0
	ShotCost(1)=0
}
