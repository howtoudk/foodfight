/*	Corey Dixon
 *	3/25/14
 *	Sammich Class
 *	Description: A sammich pick up, Will become base food pick up class
 *
 */
 
 class Sammich extends Actor
	placeable
	ClassGroup(Food);
	
var() SoundCue PickupSound;
var() SoundCue ActivationSound;
var() bool Active;
var() float RespawnTime;
var() float DelayedActivateTime;

simulated function PostBeginPlay()
{
	if(!Active)
	{
		SetTimer(DelayedActivateTime, false, 'Activate');
	}
}

function Activate()
{
	Active = true;
	SetHidden(false);
	if(ActivationSound != none)
		PlaySound(ActivationSound);
}
 
simulated event Touch( Actor Other, PrimitiveComponent OtherComp, Vector HitLocation, Vector HitNormal )
{	

	super.Touch( Other, OtherComp, HitLocation, HitNormal );
	if(Active && Other.isA('Pawn'))
	{
		PlaySound(PickupSound);
		Active = false;
		SetHidden(true);
		SetTimer(RespawnTime, false, 'Activate');
	}
}

 defaultproperties
 {
	Active = true;
	RespawnTime = 10;
	DelayedActivateTime = 5;
	PickupSound = SoundCue'FoodFight.Audio.Noms_V2_Cue'
	ActivationSound = SoundCue'FoodFight.Audio.DefaultActivationSound_V1_Cue'
	
	begin Object Class=DynamicSpriteComponent Name=FoodSprite
		Sprite=Texture2D'EditorResources.S_Actor'
		HiddenGame=true
	end Object
	Components.Add(FoodSprite)

	Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
		StaticMesh=StaticMesh'FoodFight.Models.sandwich'
		Scale=50
	End Object
	Components.Add(StaticMeshComponent0)
	
	Begin Object Class=CylinderComponent Name=CollisionCylinder
		CollisionRadius=+0034.000000
		CollisionHeight=+0078.000000
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		BlockActors=true
		CollideActors=true
	End Object
	CollisionComponent=CollisionCylinder
	Components.Add(CollisionCylinder)
	
	bCollideActors = true;
 }
 