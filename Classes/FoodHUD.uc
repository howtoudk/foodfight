/*	Corey Dixon
 *	4/8/14
 *	FoodHUD Class
 * 	Description: HUD Class for the FoodGame
 */
 
class FoodHUD extends UTHUD;

event DrawHUD()
{

	if(PlayerOwner.Pawn != none)	// if we have a pawn
	{
		//Draws Pawns Health
		Canvas.DrawColor = GreenColor;
		Canvas.Font = class'Engine'.Static.GetLargeFont();
		Canvas.SetPos(Canvas.ClipX * 0.1, Canvas.ClipY * 0.85);
		Canvas.DrawText("Health:" @
			(PlayerOwner).Pawn.Health);
		
		//Draws Current Weapons Ammo
		if(PawnOwner.Weapon != none)
		{
			Canvas.DrawColor = GreenColor;
			Canvas.Font = class'Engine'.Static.GetLargeFont();
			Canvas.SetPos(Canvas.ClipX * 0.1, Canvas.ClipY * 0.90);
			Canvas.DrawText("Ammo:" @
				UTWeapon(PawnOwner.Weapon).GetAmmoCount());
		}
	}
}

defaultproperties
{

}