/* 	Corey Dixon
 *	3/25/14
 *	FoodPlayerController
 *	Description: Player Controller for Food Fight Game, Controls a FoodFightPawn
 *
 */
 
 class FoodPlayerController extends UTPlayerController
	ClassGroup(Food);
	
reliable client function ClientSetHUD(class<HUD> newHUDType)
{
	if(myHUD != none)
		myHUD.Destroy();
	myHUD = spawn(class'FoodHUD', self);
	`log("Spawned FoodHUD!");
}
	
function CheckAutoObjective(bool bOnlyNotifyDifferent)
{
	// overriding to prevent error messages "accessed none game"
}

defaultproperties
{
	
}